Source: calendar
Section: ocaml
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders:
 Stéphane Glondu <glondu@debian.org>,
 Mehdi Dogguy <mehdi@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 ocaml-dune,
 ocaml,
 ocaml-findlib,
 ocaml-odoc,
 libre-ocaml-dev,
 libalcotest-ocaml-dev <!nocheck>,
 dh-ocaml (>= 2)
Rules-Requires-Root: no
Standards-Version: 4.6.2
Homepage: https://github.com/ocaml-community/calendar
Vcs-Git: https://salsa.debian.org/ocaml-team/calendar.git
Vcs-Browser: https://salsa.debian.org/ocaml-team/calendar

Package: libcalendar-ocaml-dev
Architecture: any
Depends:
 ocaml-findlib,
 ${ocaml:Depends},
 ${misc:Depends}
Provides: ${ocaml:Provides}
Suggests: libcalendar-ocaml-doc
Description: OCaml library providing operations over dates and times (dev files)
 OCaml library implementing common date/time operations with
 timezones and pretty printing support.

Package: libcalendar-ocaml-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends}
Description: OCaml library providing operations over dates and times (doc)
 OCaml library implementing common date/time operations with
 timezones and pretty printing support.
 .
 This package contains the library documentation.

Package: libcalendar-ocaml
Architecture: any
Depends:
 ${ocaml:Depends},
 ${misc:Depends},
 ${shlibs:Depends}
Provides: ${ocaml:Provides}
Description: OCaml library providing operations over dates and times (runtime)
 OCaml library implementing common date/time operations with
 timezones and pretty printing support.
 .
 This package contains the shared runtime libraries.
